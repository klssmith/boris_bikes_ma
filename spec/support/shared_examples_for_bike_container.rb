require 'bike_container'

shared_examples BikeContainer do
  subject { described_class.new }

  describe 'initializations' do
    it 'has a default capacity when initialized' do
      expect(subject.capacity).to eq BikeContainer::DEFAULT_CAPACITY
    end

    it 'the capacity can be overwritten' do
      random_capacity = rand(50)
      subject = described_class.new(random_capacity)
      expect(subject.capacity).to eq random_capacity
    end

    it 'has an empty bike array' do
      expect(subject.bikes).to be_empty
    end
  end
end
