require 'support/shared_examples_for_bike_container'
require 'garage'

describe Garage do
  subject(:garage) { described_class.new }
  let(:bike) { instance_double Bike, broken: true, report_working: false }
  let(:van) { instance_double Van }

  it_behaves_like BikeContainer

  describe '#give_bikes' do
    context 'when the garage is empty' do
      it 'raises an error' do
        expect{ garage.give_bikes }.to raise_error 'Garage is empty'
      end
    end

    context 'when the garage contains bikes' do
      it 'returns and removes all bikes' do
        garage.receive_bikes([bike, bike, bike])
        expect(garage.give_bikes).to eq [bike, bike, bike]
        expect(garage.bikes).not_to include(bike)
      end
    end
  end

  describe '#receive_bikes' do
    context 'when the garage is full' do
      it 'raises an error' do
        Garage::DEFAULT_CAPACITY.times do
           garage.receive_bikes([bike])
        end
        expect{garage.receive_bikes([bike])}.to raise_error "Garage is full"
      end
    end

    context 'when the garage has space' do
      it 'stores the bikes' do
        garage.receive_bikes [bike]
        expect(garage.bikes).to include bike
      end
    end
  end
end
