require 'bike'

describe Bike do
  subject(:bike) { described_class.new }

  describe 'initialization' do
    it 'is working' do
      expect(bike).not_to be_broken
      expect(bike).to be_working
    end
  end

  describe '#broken?' do
    context 'when the bike is broken' do
      it 'returns true' do
        bike.report_broken
        expect(bike.broken?).to be true
      end
    end

    context 'when the bike is working' do
      it 'returns false' do
        expect(bike.broken?).to be false
      end
    end
  end

  describe '#working?' do
    context 'when the bike is working' do
      it 'returns true' do
        expect(bike.working?).to be true
      end
    end

    context 'when the bike is broken' do
      it 'returns false' do
        bike.report_broken
        expect(bike.working?).to be false
      end
    end
  end

  describe '#report_broken' do
    it 'changes a bikes status to broken' do
      bike.report_broken
      expect(bike).to be_broken
    end
  end

  describe '#report_working' do
    it 'changes a bikes status to working' do
      bike.report_broken
      bike.report_working
      expect(bike).to be_working
    end
  end
end
