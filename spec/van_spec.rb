require 'support/shared_examples_for_bike_container'
require 'van'

describe Van do
  subject(:van) { described_class.new }
  let(:bike) { instance_double Bike, broken?: true }
  let(:docking_station) { instance_double DockingStation }
  let(:docking_station2) { instance_double DockingStation, give_bikes: [bike]}
  let(:garage) { instance_double Garage, receive_bikes: nil }

  it_behaves_like BikeContainer

  describe '#collect_bikes' do
    context 'when the van is full' do
      it 'raises an error' do
        stored_bikes = []
        Van::DEFAULT_CAPACITY.times { stored_bikes << bike }
        allow(docking_station).to receive(:give_bikes) { stored_bikes }
        van.collect_bikes docking_station
        expect{ van.collect_bikes docking_station2 }.to raise_error "Van is full"
      end
    end

    context 'when the van has space' do
      it 'stores the bikes given' do
        van.collect_bikes(docking_station2)
        expect(van.bikes).to include(bike)
      end
    end
  end

  describe '#deliver_bikes' do
    context'when the van is empty' do
      it 'raises an error' do
        expect{ van.deliver_bikes garage }.to raise_error "Van is empty"
      end
    end

    context'when the van contains bikes' do
      it 'returns all the stored bikes' do
        van.collect_bikes(docking_station2)
        van.deliver_bikes(garage)
        expect(van.bikes).to be_empty
      end
    end
  end
end
