require 'bike'
require 'docking_station'
require 'van'
require 'garage'

describe 'feature tests' do
  let(:bike) { Bike.new }
  let(:bike2) { Bike.new }
  let(:docking_station) { DockingStation.new }
  let(:van) { Van.new }
  let(:garage) { Garage.new }

  before { bike2.report_broken }

  # As a person,
  # So that I can use a bike,
  # I'd like a docking station to release a bike.
  it 'the docking station releases a bike' do
    docking_station.dock bike
    expect(docking_station.release_bike).to eq bike
  end

  # As a person,
  # So that I can use a good bike,
  # I'd like to see if a bike is working
  it 'a bike has a status to show if it is working' do
    expect(bike.working?).to be true
    expect(bike.broken?).to be false
    bike.report_broken
    expect(bike.working?).to be false
    expect(bike.broken?).to be true
  end

  # As a member of the public
  # So I can return bikes I've hired
  # I want to dock my bike at the docking station
  it 'bikes can be docked at the docking station' do
    expect{docking_station.dock bike}.not_to raise_error
  end

  # As a member of the public
  # So I can decide whether to use the docking station
  # I want to see a bike that has been docked
  it 'the docking station shows docked bikes' do
    2.times { docking_station.dock bike }
    expect(docking_station.bikes).to eq [bike, bike]
  end

  # As a member of the public,
  # So that I am not confused and charged unnecessarily,
  # I'd like docking stations not to release bikes when there are none available.
  it 'an error is raised when trying to release a bike from an empty docking station' do
    expect{docking_station.release_bike}.to raise_error "Docking station is empty"
  end

  # As a maintainer of the system,
  # So that I can control the distribution of bikes,
  # I'd like docking stations not to accept more bikes than their capacity.
  it 'does not let a docking station accept more bikes than its capacity' do
    DockingStation::DEFAULT_CAPACITY.times { docking_station.dock bike }
    expect{docking_station.dock bike}.to raise_error "Docking station is full"
  end

  # As a system maintainer,
  # So that I can plan the distribution of bikes,
  # I want a docking station to have a default capacity of 20 bikes.
  it 'docking stations have a default capacity of 20 bikes' do
    expect(DockingStation::DEFAULT_CAPACITY).to eq 20
  end

  # As a system maintainer,
  # So that busy areas can be served more effectively,
  # I want to be able to specify a larger capacity when necessary.
  it 'allows docking stations to be given a different capacity' do
    docking_station2 = DockingStation.new(30)
    20.times { docking_station2.dock bike }
    expect{ docking_station2.dock bike }.not_to raise_error
  end

  # As a member of the public,
  # So that I reduce the chance of getting a broken bike in future,
  # I'd like to report a bike as broken when I return it.
  it 'allows a bike to be reported broken' do
    bike.report_broken
    expect(bike).to be_broken
  end

  # As a maintainer of the system,
  # So that I can manage broken bikes and not disappoint users,
  # I'd like docking stations not to release broken bikes.
  it 'docking stations do not release broken bikes' do
    docking_station.dock bike2
    expect{docking_station.release_bike}.to raise_error "No working bikes"
  end

  # As a maintainer of the system,
  # So that I can manage broken bikes and not disappoint users,
  # I'd like docking stations to accept returning bikes (broken or not).
  it 'a docking station accepts working and broken bikes' do
    expect{docking_station.dock bike}.not_to raise_error
    expect{docking_station.dock bike2}.not_to raise_error
  end

  # As a maintainer of the system,
  # So that I can manage broken bikes and not disappoint users,
  # I'd like vans to take broken bikes from docking stations and deliver them to garages to be fixed.
  it 'vans collect broken bikes from docking stations' do
    docking_station.dock bike
    docking_station.dock bike2
    van.collect_bikes docking_station
    expect(docking_station.bikes).to eq [bike]
    expect(van.bikes).to eq [bike2]
  end

  it 'vans deliver broken bikes to garages' do
    docking_station.dock bike2
    van.collect_bikes docking_station
    van.deliver_bikes garage
    expect(garage.bikes).to eq [bike2]
    expect(van.bikes).not_to eq [bike2]
  end

  # As a maintainer of the system,
  # So that I can manage broken bikes and not disappoint users,
  # I'd like vans to collect working bikes from garages and distribute them to docking stations.
  describe 'takings bikes from garages to docking stations' do
    before do
      docking_station.dock bike2
      van.collect_bikes docking_station
      van.deliver_bikes garage
      van.collect_bikes garage
    end

    it 'vans collect bikes from garages' do
      expect(bike2).to be_working
      expect(garage.bikes).not_to include(bike2)
      expect(van.bikes).to include(bike2)
    end

    it 'vans deliver bikes to docking stations' do
      expect(docking_station.bikes).not_to include(bike2)
      van.deliver_bikes docking_station
      expect(docking_station.bikes).to include(bike2)
    end
  end
end
