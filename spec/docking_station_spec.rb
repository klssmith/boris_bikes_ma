require 'support/shared_examples_for_bike_container'
require 'docking_station'

describe DockingStation do
  subject(:docking_station) { described_class.new }
  let(:bike) { instance_double Bike, working?: true, report_broken: true }
  let(:bike2) { instance_double Bike, working?: false }

  it_behaves_like BikeContainer

  describe '#dock' do
    context 'when the docking station is full' do
      it 'raises an error' do
        DockingStation::DEFAULT_CAPACITY.times { docking_station.dock bike }
        expect{ docking_station.dock bike }.to raise_error 'Docking station is full'
      end
    end

    context 'when the docking station has space' do
      it 'docks a working bike' do
        expect(docking_station.dock bike).to include(bike)
      end

      it 'docks a broken bike' do
        bike.report_broken
        expect(docking_station.dock bike).to include(bike)
      end
    end
  end

  describe '#release_bike' do
    context 'when there are working bikes available' do
      it 'releases a bike' do
        docking_station.dock bike
        expect(docking_station.release_bike).to eq bike
      end
    end

    context 'when there are only broken bikes' do
      it 'raises an error' do
        allow(bike).to receive(:working?) { false }
        docking_station.dock bike
        expect{ docking_station.release_bike }.to raise_error 'No working bikes'
      end
    end

    context 'when the docking station is empty' do
      it 'raises an error' do
        expect{ docking_station.release_bike }.to raise_error 'Docking station is empty'
      end
    end
  end

  describe '#give_bikes' do
    context 'when the docking station is empty' do
      it 'raises an error' do
        expect{ docking_station.give_bikes }.to raise_error 'Docking station is empty'
      end
    end

    context 'when the docking station contains bikes' do
      it 'returns and removes the docked broken bikes' do
        docking_station.dock bike
        docking_station.dock bike2
        expect(docking_station.give_bikes).to eq [bike2]
        expect(docking_station.bikes).not_to include(bike2)
      end
    end
  end

  describe '#receive bikes' do
    context 'when the docking station is full' do
      it 'raises an error' do
        described_class::DEFAULT_CAPACITY.times { docking_station.receive_bikes([bike]) }
        expect{ docking_station.receive_bikes([bike]) }.to raise_error 'Docking station is full'
      end
    end

    context 'when the docking station has space' do
      it 'stores the bikes' do
        docking_station.receive_bikes([bike, bike])
        expect(docking_station.bikes).to eq [bike, bike]
      end
    end
  end
end
