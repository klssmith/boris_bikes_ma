[![Coverage Status](https://coveralls.io/repos/github/klssmith/Boris_Bikes/badge.svg?branch=master)](https://coveralls.io/github/klssmith/Boris_Bikes?branch=master)
[![Build Status](https://travis-ci.org/klssmith/Boris_Bikes.svg?branch=master)](https://travis-ci.org/klssmith/Boris_Bikes)

#Boris Bikes

This repository contains work done as Week One of the Makers Academy course. The different branches show work done on different days and with different pair partners.

It is a Ruby program, tested with RSpec, that simulates bikes, docking stations, vans (for collecting bikes) and garages (for fixing bikes). 
