require_relative 'bike_container'

class Garage
  include BikeContainer

  def give_bikes
    raise "Garage is empty" if empty?
    fix_bikes
    fixed_bikes = bikes
    @bikes = []
    fixed_bikes
  end

  def receive_bikes(delivered_bikes)
    raise "Garage is full" if full?
    delivered_bikes.each { |bike| bikes << bike }
  end

  private

  def fix_bikes
    bikes.each { |item| item.report_working }
  end
end
