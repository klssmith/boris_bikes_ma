class Bike
  attr_reader :broken

  def initialize
    @broken = false
  end

  def broken?
    broken
  end

  def working? #consider deleting this
    !broken
  end

  def report_broken
    @broken = true
  end

  def report_working
    @broken = false
  end
end
