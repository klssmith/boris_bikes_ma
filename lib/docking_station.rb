require_relative 'bike_container'

class DockingStation
  include BikeContainer

  def dock(bike)
    raise "Docking station is full" if full?
    bikes << bike
  end

  def release_bike
    raise "Docking station is empty" if empty?
    raise "No working bikes" unless any_working_bikes?
    bikes.each do |bike|
      return bikes.delete_at(bikes.index bike) if bike.working?
    end
  end

  def give_bikes
    raise "Docking station is empty" if empty?
    broken_bikes = bikes.reject {|bike| bike.working? }
    bikes.select! {|bike| bike.working? }
    broken_bikes
  end

  def receive_bikes(delivered_bikes)
    raise "Docking station is full" if full?
    delivered_bikes.each { |bike| bikes << bike }
  end

  private

  def any_working_bikes?
    bikes.any? { |bike| bike.working? }
  end
end
