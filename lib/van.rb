require_relative 'bike_container'

class Van
  include BikeContainer

  def collect_bikes(location)
    raise 'Van is full' if full?
    location.give_bikes.each do |bike|
      bikes << bike
    end
  end

  def deliver_bikes(location)
    raise 'Van is empty' if empty?
    location.receive_bikes(bikes)
    @bikes = []
  end
end
